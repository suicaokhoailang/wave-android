package com.example.khoi.wavepainter;

import android.graphics.PointF;
import android.util.Log;
import com.example.khoi.wavepainter.view.WavePath;

import java.util.LinkedList;

public class Visualizer {
    private static final String TAG = "Visualizer";
    private float[][] samples;
    private float[][] reducedSamples;
    private int nSamples = 480;
    private LinkedList<WavePath> waveForm;
    private int width, height;

    public Visualizer() {
        width = 0;
        height = 0;

        waveForm = new LinkedList<WavePath>();
    }

    public Visualizer(float[][] samples, int width, int height) {
        this();
        this.samples = samples;
        setViewSize(width, height);
    }

    public void setSamples(float[][] samples) {
        this.samples = samples;
    }

    public void reduceSamples() {
        reduceSamples(samples);
    }

    private void reduceSamples(float[][] samples) {
        int chunks = 2 * samples[0].length / (nSamples);
        if (chunks >= 2) {
            reducedSamples = new float[samples.length][nSamples];
            int nSelectedSamples = 0;
            for (int i = 0; i < samples.length; i++) {
                for (int j = 0; j < nSamples; j += 2) {
                    try {
                        chunks = 2 * (samples[0].length - nSelectedSamples) / (nSamples - j);
                        reducedSamples[i][j] = findMax(samples[i], nSelectedSamples, nSelectedSamples + chunks);
                        reducedSamples[i][j + 1] = findMin(samples[i], nSelectedSamples, nSelectedSamples + chunks);
                        nSelectedSamples += chunks;
                    } catch (ArrayIndexOutOfBoundsException aioe) {
                        Log.w("reduce", "out of bounds");
                    }
                }
                nSelectedSamples = 0;
            }

        } else {
            reducedSamples = new float[samples.length][samples[0].length];
            for (int i = 0; i < samples.length; i++) {
                System.arraycopy(samples[i], 0, reducedSamples[i], 0, samples[0].length);
            }
        }

    }

    private float findMax(float samples[], int i, int j) {
        float max = samples[i];
        for (int count = i; count < j; count++) {
            if (samples[count] > max) {
                max = samples[count];
            }
        }
        return max;
    }

    private float findMin(float samples[], int i, int j) {
        float min = samples[i];
        for (int count = i; count < j; count++) {
            if (samples[count] < min) {
                min = samples[count];
            }
        }
        return min;
    }

    private float computeY(float sample, int channel, int channels) {
        if (channel == 0) {
            return (float) height * (sample + channel + 1) / 2 / channels;
        }
        return (float) height / channels + computeY(sample, channel - 1, channels);
    }

    private LinkedList<WavePath> makeForm() {
        int len = reducedSamples[0].length;
        Log.w(TAG, "len = " + len);
        float distance = (float) (2 * width) / len;
        int channels = reducedSamples.length;
        float x;
        if (distance <= 1) {
            for (int channel = 0; channel < reducedSamples.length; channel++) {
                x = 0.0f;
                float[] reducedSample = reducedSamples[channel];
                for (int i = 0; i < len; i += 2) {
                    WavePath path = new WavePath();
                    path.setBegin(new PointF(x, computeY(reducedSample[i], channel, channels)));
                    path.setEnd(new PointF(x, computeY(reducedSample[i + 1], channel, channels)));
                    path.moveTo(path.getBegin().x, path.getBegin().y);
                    path.lineTo(path.getEnd().x, path.getEnd().y);
                    waveForm.add(path);
                    x += distance;
                }
            }
        } else if (len > 0) {
            distance = (float) width / len;
            float oldX, oldY, newX, newY;
            for (int channel = 0; channel < reducedSamples.length; channel++) {
                x = 0.0f;
                WavePath path = new WavePath();
                float[] reducedSample = reducedSamples[channel];
                oldX = 0;
                oldY = computeY(reducedSample[0], channel, channels);
                path.moveTo(oldX, oldY);
                for (int i = 0; i < len; i++) {
                    x += distance;
                    newX = x;
                    newY = computeY(reducedSample[i], channel, channels);
                    path.quadTo(oldX, oldY, (newX + oldX) / 2, (newY + oldY) / 2);
                    oldX = newX;
                    oldY = newY;
                }
                waveForm.add(path);
            }

        } else {
            waveForm.add(new WavePath());
        }
        return waveForm;
    }

    public LinkedList<WavePath> getWaveForm() {
        waveForm = new LinkedList<WavePath>();
        return makeForm();
    }

    public void setViewSize(int width, int height) {
        this.width = width;
        this.height = height;
        nSamples = 2 * width;
        reducedSamples = new float[samples.length][nSamples];
    }
}
