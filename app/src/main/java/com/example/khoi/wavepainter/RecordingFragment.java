package com.example.khoi.wavepainter;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.media.AudioFormat;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.example.khoi.wavepainter.data.Settings;
import com.example.khoi.wavepainter.data.WaveRecorder;


public class RecordingFragment extends Fragment implements View.OnClickListener {
    private TextView recordingStatus;
    private Chronometer timer;
    private boolean isRecording;
    private WaveRecorder waveRecorder;
    private ProgressDialog progressDialog;
    private ImageButton recordBtn;
    private RadioButton r1,r2,r3,r4;
    private TextView modeBtn;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(
                R.layout.recording_fragment, container, false);
        isRecording = false;
        recordBtn = (ImageButton)view.findViewById(R.id.recording_btn);
        recordingStatus = (TextView)view.findViewById(R.id.recording_status);
        timer = (Chronometer)view.findViewById(R.id.chronometer);
        timer.stop();
        recordingStatus.setText("Touch the icon");
        recordBtn.setOnClickListener(this);
        r1 = (RadioButton)view.findViewById(R.id.r1);
        r2 = (RadioButton)view.findViewById(R.id.r2);
        r3 = (RadioButton)view.findViewById(R.id.r3);
        r4 = (RadioButton)view.findViewById(R.id.r4);
        modeBtn = (TextView)view.findViewById(R.id.mode_btn);
        modeBtn.setOnClickListener(this);
        initializeRadioButtonListeners();
        return view;
    }

    private void initializeRadioButtonListeners() {
        r1.setOnClickListener(this);
        r2.setOnClickListener(this);
        r3.setOnClickListener(this);
        r4.setOnClickListener(this);
    }


    public RecordingFragment() {
        super();
    }
    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.recording_btn:
                Log.w("Record","Clicked");
                record();
                break;
            case R.id.r1:
                r1.setChecked(true);
                r2.setChecked(false);
                r3.setChecked(false);
                r4.setChecked(false);
                Settings.sampleRate = 44100;
                break;
            case R.id.r2:
                r2.setChecked(true);
                r1.setChecked(false);
                r3.setChecked(false);
                r4.setChecked(false);
                Settings.sampleRate = 22050;
                break;
            case R.id.r3:
                r3.setChecked(true);
                r2.setChecked(false);
                r1.setChecked(false);
                r4.setChecked(false);
                Settings.sampleRate = 11025;
                break;
            case R.id.r4:
                r4.setChecked(true);
                r2.setChecked(false);
                r3.setChecked(false);
                r1.setChecked(false);
                Settings.sampleRate = 8000;
                break;
            case R.id.mode_btn:
                String text = (String)modeBtn.getText();
                if(text.equals("Mono")){
                    modeBtn.setText("Stereo");
                    Settings.audioFormat = AudioFormat.CHANNEL_IN_STEREO;
                }else {
                    modeBtn.setText("Mono");
                    Settings.audioFormat = AudioFormat.CHANNEL_IN_MONO;
                }
                break;
            default:
                break;
        }
    }
    public void record() {
        try {
            if (!isRecording) {
                waveRecorder = new WaveRecorder();
                timer.setBase(SystemClock.elapsedRealtime());
                timer.start();
                recordBtn.setImageResource(R.drawable.record_started_large);
                isRecording = true;
                recordingStatus.setText("Recording");
                waveRecorder.startRecording();
            } else {
                stop();
            }
        } catch (IllegalStateException ise) {
            ise.printStackTrace();
        }
    }
    public void stop(){
        timer.stop();
        recordBtn.setImageResource(R.drawable.record_large);
        progressDialog = ProgressDialog.show(getActivity(), "Please wait ...", "Saving  ...", true);
        progressDialog.setCancelable(false);
        Thread th = new Thread(new Runnable() {
            @Override
            public void run() {
                waveRecorder.stopRecording();
                progressDialog.dismiss();
            }

        });
        th.start();
        recordingStatus.setText("Done");
        recordingStatus.invalidate();
        isRecording = false;
    }
    public boolean isRecording(){
        return isRecording;
    }

}
