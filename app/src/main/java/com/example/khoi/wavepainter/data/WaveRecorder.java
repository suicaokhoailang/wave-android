package com.example.khoi.wavepainter.data;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Environment;
import android.util.Log;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

public class WaveRecorder implements Runnable {
    private AudioRecord recorder;
    private int sampleRate;
    private int audioFormat;
    private int bufferSize;
    /*File used to store raw audio data*/
    private File rawFile;
    /*Actual wave file*/
    private File waveFile;
    private Thread recordingThread;
    private boolean isRecording;
    private int channels;
    private int longSampleRate;
    private long totalDataLen;
    private long totalAudioLen;

    public WaveRecorder() {
        File dir = new File(Environment.getExternalStorageDirectory().
                getAbsolutePath() + "/WavePainter/Records");
        if (!dir.exists()) {
            dir.mkdirs();
        }
        rawFile = new File(dir + "/" + "myrecording.raw");

        String date = new SimpleDateFormat("dd-MM-yyyy HH-mm-ss").format(
                new Date(System.currentTimeMillis())
        );
        waveFile = new File(dir + "/" + "Recording "+date + ".wav");
        setTempFile(rawFile);
        setWaveFile(waveFile);
        sampleRate = Settings.sampleRate;
        audioFormat = Settings.audioFormat;
    }

    public void setTempFile(String dir) {
        rawFile = new File(dir);
        if (!rawFile.exists()) {
            try {
                rawFile.createNewFile();
            } catch (IOException ioe) {
                Log.w("Recorder", "cannot create temp file");
            }
        }
    }

    public void setTempFile(File file) {
        rawFile = file;
    }

    public void setWaveFile(String dir) {
        waveFile = new File(dir);
        if (!waveFile.exists()) {
            try {
                waveFile.createNewFile();
            } catch (IOException ioe) {
                Log.w("Recorder", "cannot create wavefile");
            }
        }
    }

    public void setWaveFile(File file) {
        waveFile = file;
    }

    public void startRecording() {
        bufferSize = AudioRecord.getMinBufferSize(sampleRate,
                audioFormat,
                AudioFormat.ENCODING_PCM_16BIT);
        recorder = new AudioRecord(MediaRecorder.AudioSource.MIC, sampleRate, audioFormat, AudioFormat.ENCODING_PCM_16BIT, bufferSize);
        if (recorder.getState() == 1) {
            recorder.startRecording();
        }
        isRecording = true;
        recordingThread = new Thread(new Runnable() {
            @Override
            public void run() {
                writeRawAudioData();
            }
        });
        recordingThread.start();
    }

    private void writeRawAudioData() {
        Log.w("Record", "buffer size = " + bufferSize);
        byte data[] = new byte[bufferSize];
        String fileName = rawFile.getAbsolutePath();
        FileOutputStream os = null;

        try {
            os = new FileOutputStream(fileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        int read = 0;

        if (null != os) {
            while (isRecording) {
                read = recorder.read(data, 0, bufferSize);

                if (AudioRecord.ERROR_INVALID_OPERATION != read) {
                    try {
                        os.write(data);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            try {
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void stopRecording() {
        if (null != recorder) {
            isRecording = false;

            if (recorder.getState() == 1) {
                recorder.stop();

            }
            recorder.release();
            longSampleRate = recorder.getSampleRate();
            channels = recorder.getChannelCount();
            recorder = null;
            recordingThread = null;
        }

        copyWaveFile(rawFile, waveFile);
        deleteTempFile();
    }

    private void deleteTempFile() {
        rawFile.delete();
    }

    private void copyWaveFile(File inFilename, File outFilename) {
        FileInputStream in = null;
        FileOutputStream out = null;
        totalAudioLen = 0;
        totalDataLen = totalAudioLen + 36;
        bufferSize = 16 * sampleRate * channels / 8;

        byte[] data = new byte[bufferSize];

        try {
            in = new FileInputStream(inFilename);
            out = new FileOutputStream(outFilename);
            totalAudioLen = in.getChannel().size();
            totalDataLen = totalAudioLen + 36;

            Log.w("Recording", "File size: " + totalDataLen);

            out.write(WaveRecorder.writeWaveFileHeader(totalAudioLen, totalDataLen,
                    longSampleRate, channels, bufferSize), 0, 44);

            while (in.read(data) != -1) {
                out.write(data);
            }

            in.close();
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static byte[] writeWaveFileHeader(
            long totalAudioLen,
            long totalDataLen, long longSampleRate, int channels,
            long byteRate) throws IOException {

        byte[] header = new byte[44];

        header[0] = 'R';  // RIFF/WAVE header
        header[1] = 'I';
        header[2] = 'F';
        header[3] = 'F';
        header[4] = (byte) (totalDataLen & 0xff);
        header[5] = (byte) ((totalDataLen >> 8) & 0xff);
        header[6] = (byte) ((totalDataLen >> 16) & 0xff);
        header[7] = (byte) ((totalDataLen >> 24) & 0xff);
        header[8] = 'W';
        header[9] = 'A';
        header[10] = 'V';
        header[11] = 'E';
        header[12] = 'f';  // 'fmt ' chunk
        header[13] = 'm';
        header[14] = 't';
        header[15] = ' ';
        header[16] = 16;  // 4 bytes: size of 'fmt ' chunk
        header[17] = 0;
        header[18] = 0;
        header[19] = 0;
        header[20] = 1;  // format = 1
        header[21] = 0;
        header[22] = (byte) channels;
        header[23] = 0;
        header[24] = (byte) (longSampleRate & 0xff);
        header[25] = (byte) ((longSampleRate >> 8) & 0xff);
        header[26] = (byte) ((longSampleRate >> 16) & 0xff);
        header[27] = (byte) ((longSampleRate >> 24) & 0xff);
        header[28] = (byte) (byteRate & 0xff);
        header[29] = (byte) ((byteRate >> 8) & 0xff);
        header[30] = (byte) ((byteRate >> 16) & 0xff);
        header[31] = (byte) ((byteRate >> 24) & 0xff);
        header[32] = (byte) (2 * 16 / 8);  // block align
        header[33] = 0;
        header[34] = 16;  // bits per sample
        header[35] = 0;
        header[36] = 'd';
        header[37] = 'a';
        header[38] = 't';
        header[39] = 'a';
        header[40] = (byte) (totalAudioLen & 0xff);
        header[41] = (byte) ((totalAudioLen >> 8) & 0xff);
        header[42] = (byte) ((totalAudioLen >> 16) & 0xff);
        header[43] = (byte) ((totalAudioLen >> 24) & 0xff);

        return header;
    }

    @Override
    public void run() {
        startRecording();
    }
}
