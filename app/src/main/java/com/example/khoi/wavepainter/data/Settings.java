package com.example.khoi.wavepainter.data;

import android.media.AudioFormat;

public class Settings {
    public static int sampleRate = 44100;
    public static int audioFormat = AudioFormat.CHANNEL_IN_MONO;
}
