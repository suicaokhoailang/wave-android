package com.example.khoi.wavepainter;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;


public class MainActivity extends Activity {
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    private PlayingFragment playingFragment;
    private RecordingFragment recordingFragment;
    private VisualizationFragment visualizationFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentManager = getFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        playingFragment = new PlayingFragment();
        recordingFragment = new RecordingFragment();
        visualizationFragment = new VisualizationFragment();
        fragmentTransaction.replace(android.R.id.content, recordingFragment);
        fragmentTransaction.commit();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        fragmentTransaction = fragmentManager.beginTransaction();
        if (id == R.id.action_playing) {
            if(recordingFragment.isRecording()){
                recordingFragment.stop();
            }
            fragmentTransaction.replace(android.R.id.content, playingFragment);
        } else {
            visualizationFragment.stop();
            fragmentTransaction.replace(android.R.id.content, recordingFragment);
        }
        fragmentTransaction.commit();
        return super.onOptionsItemSelected(item);
    }

    public VisualizationFragment getVisualizationFragment(){
        return visualizationFragment;
    }
}
