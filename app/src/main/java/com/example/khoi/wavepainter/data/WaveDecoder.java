package com.example.khoi.wavepainter.data;

import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class WaveDecoder {
    private static final String TAG = "WaveDecoder";
    private final float MAX_VALUE = 1.0f / Short.MAX_VALUE;
    private File inputFile;
    private int totalDataSize;
    private short channels;
    private int sampleRate;
    private int bufferSize;
    private byte buffer[];
    private short bitsPerSample;

    //TODO suppot mono/stereo
    private float[][] computedSamples;
    private short[][] samples;
    private FileInputStream fis;

    public WaveDecoder() {

    }

    public WaveDecoder(String fileName) throws FileNotFoundException {
        fis = new FileInputStream(fileName);
    }

    public WaveDecoder(File file) throws FileNotFoundException {
        this.inputFile = file;
        fis = new FileInputStream(inputFile);
    }

    public void setInputFile(String inputFileName) {
        inputFile = new File(inputFileName);
        try {
            if (!inputFile.exists()) {
                inputFile.createNewFile();
            }
            fis = new FileInputStream(inputFile);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public void setInputFile(File file) {
        inputFile = file;
        try {
            fis = new FileInputStream(inputFile);
        } catch (FileNotFoundException fnfe) {
            fnfe.printStackTrace();
        }
    }

    private String read4ByteString() throws IOException {
        StringBuffer str = new StringBuffer("");
        for (int i = 0; i < 4; i++) {
            try {
                str.append((char) fis.read());
            } catch (NullPointerException npe) {
                Log.w("Decode", fis + "");
            }
        }
        return str.toString();
    }

    private int readIntLittleEndian() throws IOException {
        byte data[] = new byte[4];
        ByteBuffer bb = ByteBuffer.allocate(4);
        for (int i = 0; i < 4; i++) {
            data[i] = (byte) fis.read();
            bb = ByteBuffer.wrap(data);
        }
        return bb.order(ByteOrder.LITTLE_ENDIAN).getInt();
    }

    private short readShortLittleEndian() throws IOException {
        byte b1 = (byte) (fis.read() & 0xff);
        byte b2 = (byte) (fis.read() & 0xff);
        byte[] data = {b1, b2};
        return ByteBuffer.wrap(data).order(ByteOrder.LITTLE_ENDIAN).getShort();
    }

    public void decode() throws IOException {
        decodeHeader();
        decodeData();
    }

    private void decodeHeader() throws IOException {
        Log.w("Decode", "" + read4ByteString());
        Log.w("Decode", "" + (totalDataSize = readIntLittleEndian()));
        Log.w("Decode", "" + read4ByteString());
        Log.w("Decode", "" + read4ByteString());
        Log.w("Decode", "" + readIntLittleEndian());
        Log.w("Decode", "" + readShortLittleEndian());
        Log.w("Decode", "" + (channels = readShortLittleEndian()));
        Log.w("Decode", "" + (sampleRate = readIntLittleEndian()));
        Log.w("Decode", "" + readIntLittleEndian());
        Log.w("Decode", "" + readShortLittleEndian());
        Log.w("Decode", "" + (bitsPerSample = readShortLittleEndian()));
        Log.w("Decode", "" + read4ByteString());
        Log.w("Decode", "" + readIntLittleEndian());
    }

    private void decodeData() throws IOException {
        buffer = new byte[getTotalSoundDataSize()];
        fis.read(buffer);
        fis.close();
        samples = new short[channels][getTotalSoundDataSize() / (channels * bitsPerSample / 8)];
        //markSound();
        int len = samples[0].length;
        int j = 0;
        /*
        * Mono
        * */
        if (channels == 1) {
            for (int i = 0; i < len; i++, j += 2) {
                try {
                    byte b1 = (byte) (buffer[j] & 0xff);
                    byte b2 = (byte) (buffer[j + 1] & 0xff);
                    byte[] data = {b1, b2};
                    samples[0][i] = ByteBuffer.wrap(data).order(ByteOrder.LITTLE_ENDIAN).getShort();
                    samples[0][i] = (short) (-samples[0][i]);
                } catch (IndexOutOfBoundsException iobe) {
                    Log.w("WaveRecorder", "Out of bounds somehow");
                    break;
                }
            }
        } else if (channels == 2) {
            for (int i = 0; i < len; i++, j += 4) {
                try {
                    byte b1 = (byte) (buffer[j] & 0xff);
                    byte b2 = (byte) (buffer[j + 1] & 0xff);
                    byte b3 = (byte) (buffer[j + 2] & 0xff);
                    byte b4 = (byte) (buffer[j + 3] & 0xff);
                    byte[] leftData = {b1, b2};
                    samples[0][i] = ByteBuffer.wrap(leftData).order(ByteOrder.LITTLE_ENDIAN).getShort();
                    samples[0][i] = (short) (-samples[0][i]);
                    byte[] rightData = {b3, b4};
                    samples[1][i] = ByteBuffer.wrap(rightData).order(ByteOrder.LITTLE_ENDIAN).getShort();
                    samples[1][i] = (short) (-samples[1][i]);
                } catch (IndexOutOfBoundsException iobe) {
                    Log.w("WaveRecorder", "Out of bounds somehow");
                    break;
                }
            }
            Log.w(TAG, "length = " + samples[0].length);
        } else {
            throw new IllegalStateException();
        }
    }


    public float[][] getComputedSamples() {
        computedSamples = new float[channels][samples[0].length];
        int length = samples[0].length;
        for (int i = 0; i < channels; i++) {
            for (int j = 0; j < length; j++) {
                computedSamples[i][j] = samples[i][j] * MAX_VALUE;
            }
        }
        return computedSamples;
    }

    public short[][] getSubSamples(double bRatio, double eRatio) {
        int begin = (int) (samples[0].length * bRatio);
        int end = (int) (samples[0].length * eRatio);
        return getSubSamples(samples, begin, end);
    }

    private short[][] getSubSamples(short samples[][], int begin, int end) {
        int newLen = end - begin;
        short subSamples[][] = new short[channels][newLen];
        for (int i = begin, j = 0; i < end; i++, j++) {
            for (int channel = 0; channel < channels; channel++) {
                try {
                    subSamples[channel][j] = samples[channel][i];
                } catch (ArrayIndexOutOfBoundsException aobe) {
                    break;
                }
            }
        }
        return subSamples;
    }

    public short[][] getSamples() {
        return samples;
    }

    private short findMax(short samples[], int i, int j) {
        short max = samples[i];
        for (int count = i; count < j; count++) {
            if (samples[count] > max) {
                max = samples[count];
            }
        }
        return max;
    }

    private float findMaxFloat(float samples[], int i, int j) {
        float max = samples[i];
        for (int count = i; count < j; count++) {
            if (samples[count] > max) {
                max = samples[count];
            }
        }
        return max;
    }


    private float[][] compute(short[][] samples) {
        int length = samples[0].length;
        float[][] computedSamples = new float[channels][length];
        for (int i = 0; i < channels; i++) {
            for (int j = 0; j < length; j++) {
                computedSamples[i][j] = (float) samples[i][j] * MAX_VALUE;
            }
        }
        return computedSamples;
    }

    /*public void markSound() {
        float  playTime = (float)getTotalSoundDataSize()/getByteRate();
        int sampleBounds = (int) (samples[0].length*(Settings.soundTimeThreshold/playTime/1000));
        soundMarkerList = new Marker[channels][(int)(playTime/Settings.soundTimeThreshold*1000)];
        for(int i = 0; i < channels; i++){
            markSound(i, sampleBounds);
        }
    }

    private void markSound(int channel, int sampleBounds) {
        if (channel > channels - 1) return;
        getComputedSamples();
        float samples[] = computedSamples[channel];
        int length = samples.length;
        int k = 0;
        for(int i = 0; i < length; i ++){
            //Nếu phát hiện âm > threshold, kiểm tra tiếp điều kiện 2
            if(Math.abs(samples[i]) > Settings.soundAmpThreshold){
                int j = i + 1;
                while(j < length - 2){
                    //Chỉ quan tâm đến cá điểm
                    if(Math.abs(samples[j]) > Math.abs(samples[j + 1]) && Math.abs(samples[j]) > Math.abs(samples[j - 1])){
                        if(Math.abs(samples[j]) < Settings.soundAmpThreshold){
                            break;
                        }
                    }
                    j++;
                }
                Log.w(TAG,"Bounds =  "+(j - i));
                if(j - i > sampleBounds){
                    Log.w(TAG,"Found "+i+" "+j);
                    Marker m = new Marker((double)i/length,(double)j/length);
                    soundMarkerList[channel][k] = m;
                    k++;
                }
                i += j - 1;
            }
        }
    }

    public void markSilence() {
        float  playTime = (float)getTotalSoundDataSize()/getByteRate();
        int sampleBounds = (int) (samples[0].length*(Settings.silenceTimeThreshold/playTime/1000));
        silenceMarkerList = new Marker[channels][(int)(playTime/Settings.silenceTimeThreshold*1000)];
        Log.w(TAG, silenceMarkerList.length+" ");
        for(int i = 0; i < channels; i++){
            markSilence(i, sampleBounds);
        }
    }

    private void markSilence(int channel, int sampleBounds) {
        if (channel > channels - 1) return;
        getComputedSamples();
        float samples[] = computedSamples[channel];
        int length = samples.length;
        int k = 0;
        for(int i = 0; i < length; i ++){
            //Nếu phát hiện âm < threshold, kiểm tra tiếp điều kiện 2
            if(Math.abs(samples[i]) < Settings.silenceAmpThreshold){
                int j = i;
                while(j < length - 1 && Math.abs(samples[j]) < Settings.silenceAmpThreshold){
                    j++;
                }
                if(channel == 0){
                    Log.w(TAG,"Bounds = "+(j - i));
                }
                if(j - i > sampleBounds){
                    if(channel == 0){
                        Log.w(TAG, "Found + "+i+" "+j);
                    }
                    Marker m = new Marker((double)i/length,(double)j/length);
                    silenceMarkerList[channel][k] = m;
                    k++;
                    Log.w(TAG,m.getLeft()+" "+m.getRight());
                }
                i += j;
            }
        }
    }

    public Marker[][] getSilenceMarkerList() {
        return silenceMarkerList;
    }

    public Marker[][] getSoundMarkerList() {
        return soundMarkerList;
    }
*/
    public int getTotalDataSize() {
        return this.totalDataSize;
    }

    public int getSampleRate() {
        return this.sampleRate;
    }

    public short getChannels() {
        return this.channels;
    }

    public short getBitsPerSample() {
        return this.bitsPerSample;
    }

    public int getByteRate() {
        return sampleRate * channels * bitsPerSample / 8;
    }

    public int getBlockAlign() {
        return channels * bitsPerSample / 8;
    }

    public int getTotalSoundDataSize() {
        return totalDataSize - 36;
    }

    public File getInputFile() {
        return inputFile;
    }

    public float[][] getComputedSubSamples(double leftRatio, double rightRatio) {
        return compute(getSubSamples(leftRatio, rightRatio));
    }

    private enum MarkerState {
        BEGIN,END
    }
}
