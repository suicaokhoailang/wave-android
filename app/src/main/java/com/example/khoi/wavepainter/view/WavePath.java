package com.example.khoi.wavepainter.view;

import android.graphics.Path;
import android.graphics.PointF;

/**
 * Created by khoi on 11/14/2014.
 */
public class WavePath extends Path {
    private PointF begin;
    private PointF end;
    public WavePath() {
    }
    public PointF getBegin() {
        return begin;
    }

    public void setBegin(PointF begin) {
        this.begin = begin;
    }

    public PointF getEnd() {
        return end;
    }

    public void setEnd(PointF end) {
        this.end = end;
    }

}
