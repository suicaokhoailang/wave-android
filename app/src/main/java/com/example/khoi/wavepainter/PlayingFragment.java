package com.example.khoi.wavepainter;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.example.khoi.wavepainter.data.WaveDecoder;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by khoi on 9/9/2014.
 */
public class PlayingFragment extends Fragment {
    private ScrollView fileListView;
    private LinearLayout fileListLayout;
    private WaveDecoder decoder;
    private final long
        x10 = 1024,
        x20 = 1048576,
        x30 = 1073741824;
    private File dir;
    int colors[] = {0xffc5e5fe, 0xff8bcbfe, 0xffdfd8be, 0xfffca1a1, 0xfffc89a1, 0xffc989bd, 0xff69248f};
    int colorIndex = 0;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(
                R.layout.playing_fragment, container, false);
        fileListLayout = (LinearLayout) view.findViewById(R.id.playing_layout);
        fileListView = (ScrollView) view.findViewById(R.id.file_list);
        dir = new File(Environment.getExternalStorageDirectory().
                getAbsolutePath() + "/WavePainter/Records");
        if (!dir.exists()) {
            dir.mkdirs();
        }
        File[] records = dir.listFiles();
        for (File record : records) {
            String date = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(
                    new Date(record.lastModified())
            );
            long length = record.length();
            String convertedLength;
            if(length < x10){
                convertedLength = length+"B";
            }
            else if(length< x20){
                convertedLength = length/x10+"KB";
            }
            else if(length<x30){
                convertedLength=length/x20+"MB";
            }
            else{
                convertedLength=length/x30+"GB";
            }
            createNewRecordView(record.getAbsolutePath(), record.getName(),date, convertedLength);
        }
        view.invalidate();
        return view;
    }


    private LinearLayout createNewRecordView(final String fileName, String name, final String date, String length) {
        LinearLayout ll = new LinearLayout(this.getActivity());
        ll.setOrientation(LinearLayout.VERTICAL);
        ll.setPadding(0, 0, 10, 0);
        ll.setBackgroundColor(colors[colorIndex]);
        ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                decoder = new WaveDecoder();
                decoder.setInputFile(fileName);
                Log.w("Tag", fileName);
                beginVisualizing();
            }
        });
        if (colorIndex == colors.length - 1) {
            colorIndex = 0;
        } else {
            colorIndex++;
        }
        LinearLayout.LayoutParams lParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 80);
        ll.setLayoutParams(lParams);
        fileListLayout.addView(ll);

        RelativeLayout rl = new RelativeLayout(this.getActivity());
        RelativeLayout.LayoutParams rParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        rl.setLayoutParams(rParams);
        rl.setBackgroundColor(0xffe1e1e1);
        ll.addView(rl);

        TextView nameText = new TextView(this.getActivity());
        nameText.setText(name);
        nameText.setPadding(20, 20, 0, 0);
        nameText.setTextColor(0xff3d3d3d);
        nameText.setTextSize(15);
        ViewGroup.LayoutParams textParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        nameText.setLayoutParams(textParams);

        TextView dateText = new TextView(this.getActivity());
        dateText.setText(date);
        dateText.setTextColor(0xff3d3d3d);
        dateText.setTextSize(10);
        dateText.setPadding(20, 50, 0, 0);
        dateText.setLayoutParams(textParams);

        TextView lengthText = new TextView(this.getActivity());
        lengthText.setLayoutParams(textParams);
        lengthText.setText(length);
        lengthText.setTextColor(0xff3d3d3d);
        lengthText.setTextSize(10);
        lengthText.setPadding(400, 25, 0, 0);

        rl.addView(nameText);
        rl.addView(dateText);
        rl.addView(lengthText);

        return ll;
    }
    public void beginVisualizing() {
        FragmentTransaction fragmentTransaction = getActivity().getFragmentManager().beginTransaction();
        final MainActivity mainActivity = (MainActivity)getActivity();
        final VisualizationFragment visualizationFragment = mainActivity.getVisualizationFragment();
        fragmentTransaction.replace(android.R.id.content, mainActivity.getVisualizationFragment());
        fragmentTransaction.commit();
        final ProgressDialog progressDialog = ProgressDialog.show(getActivity(), "Please wait ...", "Decoding  ...", true);
        progressDialog.setCancelable(false);
        final Thread th = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    decoder.decode();
                    Visualizer visualizer = new Visualizer(decoder.getComputedSamples(), 320, 320);
                    visualizationFragment.setVisualizer(visualizer);
                    visualizationFragment.setDecoder(decoder);
                    mainActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            visualizationFragment.getZoomOutBtn().performClick();
                        }
                    });
                    Log.w("Decode", "Done");
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
                progressDialog.dismiss();
            }
        });
        th.start();
    }
}
