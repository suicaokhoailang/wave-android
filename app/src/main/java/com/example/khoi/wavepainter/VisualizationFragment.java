package com.example.khoi.wavepainter;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.SeekBar;
import com.example.khoi.wavepainter.data.WaveDecoder;
import com.example.khoi.wavepainter.data.WaveRecorder;
import com.example.khoi.wavepainter.view.Marker;
import com.example.khoi.wavepainter.view.WaveFormView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class VisualizationFragment extends Fragment {
    private WaveFormView waveFormView;
    private State state = State.START;
    private ImageButton zoomOutBtn, zoomInBtn, playBtn, trimBtn;

    private SeekBar playProgressBar;
    private Thread progressThread;
    private boolean threadStarted;
    private int progress;
    private WaveDecoder decoder;
    private Visualizer visualizer;
    private MediaPlayer player;
    private int currentPosition;
    private String TAG = "VisualizationFragment";

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(
                R.layout.visualization_fragment, container, false);
        state = State.START;
        waveFormView = (WaveFormView) view.findViewById(R.id.form1);
        zoomOutBtn = (ImageButton) view.findViewById(R.id.visualize_btn);
        playProgressBar = (SeekBar)view.findViewById(R.id.play_progress);
        playProgressBar.setEnabled(false);
        playProgressBar.setProgress(0);
        playProgressBar.invalidate();
        threadStarted = false;
        player = new MediaPlayer();
        zoomOutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                visualizer.setViewSize(waveFormView.getWidth(), waveFormView.getHeight());
                waveFormView.setChannels(decoder.getChannels());
                waveFormView.resetForm();
                Marker zr = waveFormView.getPreviousZoomRatio();
                visualizer.setSamples(decoder.getComputedSubSamples(zr.getLeft(), zr.getRight()));
                visualizer.reduceSamples();
                waveFormView.setWaveForm(visualizer.getWaveForm());
                waveFormView.drawWave();
            }
        });
        zoomInBtn = (ImageButton) view.findViewById(R.id.zoom_btn);
        zoomInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                visualizer.setViewSize(waveFormView.getWidth(), waveFormView.getHeight());
                waveFormView.updateZoomRatio();
                Marker zr = waveFormView.getCurrentZoomRatio();
                Log.w(TAG,zr.getLeft()+" "+zr.getRight());
                visualizer.setSamples(decoder.getComputedSubSamples(zr.getLeft(), zr.getRight()));
                visualizer.reduceSamples();
                waveFormView.setChannels(decoder.getChannels());
                waveFormView.resetForm();
                waveFormView.setWaveForm(visualizer.getWaveForm());
                waveFormView.drawWave();
            }
        });

        playBtn = (ImageButton) view.findViewById(R.id.play_btn);
        playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (state == State.START) {
                    try {
                        playBtn.setImageResource(R.drawable.pause);
                        play();
                        playProgressBar.setMax(player.getDuration());
                        if(!threadStarted){
                            progressThread = new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    progress  = 0;
                                    Runnable r = new Runnable() {
                                        @Override
                                        public void run() {
                                            playProgressBar.invalidate();
                                        }
                                    };

                                    while(progress < playProgressBar.getMax()){
                                        playProgressBar.setProgress(progress);
                                        try{
                                            getActivity().runOnUiThread(r);
                                        }catch (NullPointerException npe){
                                            Log.w(TAG,"Activity destroyed");
                                            break;
                                        }
                                        progress+=100;
                                        if(state == State.START){
                                            break;
                                        }
                                        if(state == State.PAUSE){
                                            try{
                                                Thread.sleep(Long.MAX_VALUE);
                                            }catch (InterruptedException ie){
                                                Log.w(TAG,"Insomnia");
                                            }
                                        }
                                        else{
                                            try{
                                                Thread.sleep(100);
                                            }catch (InterruptedException ie){
                                                Log.w(TAG,"Insomnia");
                                            }
                                        }
                                    }

                                    try{

                                        if(state != State.START){
                                            playProgressBar.setProgress(playProgressBar.getMax());
                                        }else{
                                            playProgressBar.setProgress(0);
                                        }
                                        getActivity().runOnUiThread(r);
                                    }
                                    catch (NullPointerException npe){
                                        Log.w(TAG,"Activity destroyed");
                                    }
                                }
                            });
                            threadStarted = true;
                            progressThread.start();
                        }
                        else{
                            progressThread.interrupt();
                        }
                        state = State.PLAY;
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }
                } else if (state == State.PLAY) {
                    try {
                        playBtn.setImageResource(R.drawable.play);
                        currentPosition = player.getCurrentPosition();
                        state = State.PAUSE;
                        pause();
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }
                } else {
                    playBtn.setImageResource(R.drawable.play);
                    progressThread.interrupt();
                    state = State.PLAY;
                    resume();
                }
            }
        });
        playBtn.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                reset();
                return true;
            }
        });


        trimBtn = (ImageButton) view.findViewById(R.id.trim_btn);
        trimBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Marker w = waveFormView.getCurrentZoomRatio();
                waveFormView.updateZoomRatio();
                Marker zr = waveFormView.getCurrentZoomRatio();
                Log.w("Trim", "" + decoder.getSamples().length);
                final short[][] samples = decoder.getSubSamples(zr.getLeft(), zr.getRight());
                waveFormView.setZoomRatio(w);
                final ProgressDialog progressDialog = ProgressDialog.show(getActivity(), "Please wait ...", "Trimming  ...", true);
                progressDialog.setCancelable(false);
                final Thread th = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            File dir = new File(Environment.getExternalStorageDirectory().
                                    getAbsolutePath() + "/WavePainter/Records");
                            String date = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(
                                    new Date(System.currentTimeMillis())
                            );
                            String fileName = "Trimmed " + date;
                            File trimmedFile = new File(dir + "/" + fileName + ".wav");
                            if (!trimmedFile.exists()) {
                                trimmedFile.createNewFile();
                            }
                            final FileOutputStream fos = new FileOutputStream(trimmedFile);
                            byte header[] = WaveRecorder.writeWaveFileHeader(2 * samples[0].length * decoder.getChannels(), 2 * samples[0].length * decoder.getChannels() + 36,
                                    decoder.getSampleRate(), decoder.getChannels(), decoder.getByteRate());
                            fos.write(header, 0, 44);
                            int len = samples[0].length;
                            int channels = decoder.getChannels();
                            byte data[] = new byte[256];
                            int k = 0;
                            try {
                                data = new byte[2*channels*len];
                                for (int i = 0; i < len; i ++) {
                                    for(int j = 0; j < channels; j++){
                                        short l = (short) -samples[j][i];
                                        data[k] = (byte) (l & 0x00ff);
                                        data[k + 1] = (byte) ((l >> 8) & 0x00ff);
                                        k += 2;
                                    }

                                }

                            } catch (ArrayIndexOutOfBoundsException aibe) {
                                Log.w(TAG, "Fuck");
                            }
                            fos.write(data, 0, data.length);
                            fos.close();

                        } catch (FileNotFoundException fnfe) {
                            fnfe.printStackTrace();
                        } catch (IOException ioe) {
                            ioe.printStackTrace();
                        }

                        progressDialog.dismiss();
                    }
                });
                th.start();
            }
        });
        return view;
    }

    private void play() throws IllegalArgumentException,
            SecurityException, IllegalStateException, IOException {
        player.reset();
        player.setDataSource(decoder.getInputFile().getAbsolutePath());
        player.prepare();
        player.start();
    }

    private void pause() throws IllegalArgumentException,
            SecurityException, IllegalStateException, IOException {
        player.pause();
    }

    private void resume() {
        player.seekTo(currentPosition);
        player.start();
    }
    public void stop() {
        if(player!= null){
            try{
                player.stop();
                player.release();
            }catch (IllegalStateException ise){
                Log.w(TAG,"Not playing");
            }
        }
    }

    private void reset(){
        if(player!=null){
            player.stop();
            player.reset();
        }
        progress = 0;
        state = State.START;
        threadStarted = false;
        playBtn.setImageResource(R.drawable.play);
        playProgressBar.setProgress(0);
    }
    public ImageButton getZoomOutBtn() {
        return zoomOutBtn;
    }

    public void setDecoder(WaveDecoder decoder) {
        this.decoder = decoder;
    }

    public void setVisualizer(Visualizer visualizer) {
        this.visualizer = visualizer;
    }



    private enum State {
        PLAY, PAUSE, START,STOP
    }


}
