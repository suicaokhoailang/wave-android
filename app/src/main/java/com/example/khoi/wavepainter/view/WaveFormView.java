package com.example.khoi.wavepainter.view;

import android.content.Context;
import android.graphics.*;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import java.util.LinkedList;


public class WaveFormView extends View implements View.OnTouchListener {
    private static final String TAG = "WaveFormView";
    private Canvas drawCanvas;
    private int channels;
    private Bitmap canvasBitmap;
    private Rect leftArea, rightArea, currentArea;
    private Paint pathPaint, canvasPaint, areaPaint, silenceMarkerPaint;
    private LinkedList<WavePath> waveForm;
    private LinkedList<Marker> ratioList;
    private LinkedList<Rect> silenceMarkerList;

    private float markerBegin, markerEnd;
    private double leftRatio, rightRatio;

    public WaveFormView(Context context, AttributeSet attrs) {
        super(context, attrs);
        ratioList = new LinkedList<Marker>();
        ratioList.add(new Marker(0,1));
        silenceMarkerList = new LinkedList<Rect>();
    }

    @Override
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        leftRatio = 0;
        rightRatio = 1;
        canvasBitmap = Bitmap.createBitmap(this.getWidth(), this.getHeight(), Bitmap.Config.ARGB_8888);
        drawCanvas = new Canvas(canvasBitmap);
        setupDrawing();
        setOnTouchListener(this);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawBitmap(canvasBitmap, 0, 0, canvasPaint);
        canvas.drawRect(leftArea, areaPaint);
        canvas.drawRect(rightArea, areaPaint);
        for(Rect r : silenceMarkerList){
            canvas.drawRect(r,silenceMarkerPaint);
        }
    }

    private void setupDrawing() {
        pathPaint = new Paint();
        pathPaint.setAntiAlias(true);
        pathPaint.setStrokeWidth(1);
        pathPaint.setStyle(Paint.Style.STROKE);
        pathPaint.setStrokeJoin(Paint.Join.ROUND);
        pathPaint.setStrokeCap(Paint.Cap.ROUND);
        pathPaint.setColor(0xff46CF19);
        silenceMarkerPaint = new Paint(pathPaint);
        silenceMarkerPaint.setColor(0x33ffffff);
        silenceMarkerPaint.setAntiAlias(false);
        silenceMarkerPaint.setStyle(Paint.Style.FILL);
        areaPaint = new Paint();
        areaPaint.setColor(0xcc8bcbfe);
        areaPaint.setStyle(Paint.Style.FILL);
        canvasPaint = new Paint(Paint.DITHER_FLAG);
        leftArea = new Rect();
        leftArea.setEmpty();
        rightArea = new Rect();
        rightArea.set(this.getWidth(), 0, this.getWidth(), this.getHeight());
    }

    public void setWaveForm(LinkedList<WavePath> waveForm) {
        this.waveForm = waveForm;
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        float touchX = event.getX();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (touchX < leftArea.right && touchX < rightArea.left) {
                    leftArea.set(0, 0, (int) touchX, this.getHeight());
                    currentArea = leftArea;
                } else if (touchX > rightArea.left && touchX > leftArea.right) {
                    rightArea.set((int) touchX, 0, this.getWidth(), this.getHeight());
                    currentArea = rightArea;
                } else {
                    if (touchX < this.getWidth() / 2) {
                        leftArea.set(0, 0, (int) touchX, this.getHeight());
                        currentArea = leftArea;
                    } else {
                        rightArea.set((int) touchX, 0, this.getWidth(), this.getHeight());
                        currentArea = rightArea;
                    }
                }
                break;
            case MotionEvent.ACTION_MOVE:
                if (currentArea == leftArea) {
                    leftArea.set(0, 0, (int) touchX < rightArea.left ? (int) touchX : rightArea.left, this.getHeight());
                } else {
                    rightArea.set((int) touchX > leftArea.right ? (int) touchX : leftArea.right, 0, this.getWidth(), this.getHeight());
                }
                break;
        }
        invalidate();
        return true;
    }

    public void resetRatio() {
        leftRatio = 0;
        rightRatio = 1;
        ratioList = new LinkedList<Marker>();
        ratioList.add(new Marker(0,1));
    }

    public void resetForm() {
        if (waveForm != null) {
            for (Path path : waveForm) {
                path.reset();
            }
        }
        silenceMarkerList = new LinkedList<Rect>();
        waveForm = new LinkedList<WavePath>();
        canvasBitmap = Bitmap.createBitmap(this.getWidth(), this.getHeight(), Bitmap.Config.ARGB_8888);
        drawCanvas.setBitmap(canvasBitmap);
        invalidate();
    }

    public void updateZoomRatio() {
        ratioList.add(new Marker(leftRatio,rightRatio));
        double reversedZoomRatio = (rightRatio - leftRatio);
        double x = (double) leftArea.right / this.getWidth() * reversedZoomRatio + leftRatio;
        rightRatio = (double) rightArea.left / this.getWidth() * reversedZoomRatio + leftRatio;
        leftRatio = x;
    }

    public Marker getPreviousZoomRatio(){
        Marker z = ratioList.getLast();
        leftRatio = z.getLeft();
        rightRatio = z.getRight();
        ratioList.removeLast();
        if(ratioList.size() == 0){
            ratioList.add(new Marker(0,1));
        }
        return z;
    }
    public Marker getCurrentZoomRatio() {
        return new Marker(leftRatio, rightRatio);
    }
    public Marker getRelativeZoomRatio(){
        return new Marker((double)leftArea.right / this.getWidth(), (double)rightArea.left/this.getWidth());
    }
    public void drawWave() {
        leftArea.setEmpty();
        rightArea.set(this.getWidth(), 0, this.getWidth(), this.getHeight());
        for (WavePath path : waveForm) {
            drawCanvas.drawPath(path, pathPaint);
        }
        invalidate();
    }
    public void addSilenceMarker(Marker marker, int channel){
        int beginY = getHeight()*channel/channels;
        int endY = beginY + getHeight()/channels;
        Rect r = new Rect((int)(marker.getLeft()*getWidth()),beginY,(int)(marker.getRight()*getWidth()),endY);
        Log.w(TAG, "" +r.left+" "+r.right);
        silenceMarkerList.add(r);
    }
    public void setChannels(int channels) {
        this.channels = channels;
    }

    public void setZoomRatio(Marker zoomRatio) {
        leftRatio = zoomRatio.getLeft();
        rightRatio = zoomRatio.getRight();
    }

}
