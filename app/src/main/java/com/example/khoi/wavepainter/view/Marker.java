package com.example.khoi.wavepainter.view;

/**
 * Created by khoi on 9/17/2014.
 */
public class Marker {
    private double left;
    private double right;

    public Marker(double l, double r) {
        left = l;
        right = r;
    }

    public void setLeft(double left) {
        this.left = left;
    }

    public void setRight(double right) {
        this.right = right;
    }

    public double getLeft() {
        return left;
    }

    public double getRight() {
        return right;
    }
}
